+++
title = "Day 2: Custom GUI Widgets with Conrod"
description = "A series on Rust GUI development"
date = 2020-01-04

draft = true
template = "page.html"

[taxonomies]
categories = ["programming"]
tags = ["rust", "gui", "conrod", "dev"]
+++

# Custom GUI Widgets with Conrod

> This is the second post in a [series](@/conrod/index.md) on GUI development in [Rust].
> If you haven't read the first post and are unfamiliar with [Conrod], I encourage you
> to go and read [that one](@/conrod/day-1.md) first.

Today, I'll get into some of the more complicated parts of Conrod,
and dig into how to create our own widgets.
I've done some experimenting on this myself with a small crate called [conrod_prompt],
so we'll rebuild some elements of that crate ourselves to start and then talk about
*the process*, as all true artists do.

## Igor, prepare the instruments

Let's do some speedy set up to kick things off.

```bash
cargo new --bin conrod_day2
cd conrod_widgets
```

Let's set up `Cargo.toml`:

```toml
[package]
name = "conrod_day2"
version = "0.1.0"
edition = "2018"

[dependencies]
conrod_core = "0.68"
conrod_glium = "0.68"
conrod_winit = "0.68"
glium = "0.24"
winit = "0.19"
```

Now, the last post ended with the following `main.rs`:

```rust
pub struct GliumDisplayWinitWrapper(pub glium::Display);

impl conrod_winit::WinitWindow for GliumDisplayWinitWrapper {
    fn get_inner_size(&self) -> Option<(u32, u32)> {
        self.0.gl_window().get_inner_size().map(Into::into)
    }
    fn hidpi_factor(&self) -> f32 {
        self.0.gl_window().get_hidpi_factor() as _
    }
}

conrod_winit::conversion_fns!();

const WIDTH: u32 = 400;
const HEIGHT: u32 = 200;

fn main() {
    // Build the window.
    let mut events_loop = glium::glutin::EventsLoop::new();
    let window = glium::glutin::WindowBuilder::new()
        .with_title("Hello, world!")
        .with_dimensions((WIDTH, HEIGHT).into());
    let context = glium::glutin::ContextBuilder::new()
        .with_vsync(true)
        .with_multisampling(4);
    let display = glium::Display::new(window, context, &events_loop).unwrap();
    let display = GliumDisplayWinitWrapper(display);

    // construct our `Ui`.
    let mut ui = conrod_core::UiBuilder::new([WIDTH as f64, HEIGHT as f64]).build();

    // Generate the widget identifiers.
    use conrod_core::widget_ids;
    widget_ids!(struct Ids { text });
    let ids = Ids::new(ui.widget_id_generator());

    // Add a `Font` to the `Ui`'s `font::Map` from file.
    let font_path = "assets/IMFePIrm29P.ttf";
    ui.fonts.insert_from_file(font_path).unwrap();

    // A type used for converting `conrod_core::render::Primitives` into `Command`s that can be used
    // for drawing to the glium `Surface`.
    let mut renderer = conrod_glium::Renderer::new(&display.0).unwrap();

    // The image map describing each of our widget->image mappings (in our case, none).
    let image_map = conrod_core::image::Map::<glium::texture::Texture2d>::new();

    let mut events = Vec::new();

    use glium::Surface;
    'render: loop {
        events.clear();

        // Get all the new events since the last frame.
        events_loop.poll_events(|event| { events.push(event); });

        // If there are no new events, wait for one.
        if events.is_empty() {
            events_loop.run_forever(|event| {
                events.push(event);
                glium::glutin::ControlFlow::Break
            });
        }

        // Process the events.
        for event in events.drain(..) {

            // Break from the loop upon `Escape` or closed window.
            match event.clone() {
                glium::glutin::Event::WindowEvent { event, .. } => {
                    match event {
                        glium::glutin::WindowEvent::CloseRequested |
                        glium::glutin::WindowEvent::KeyboardInput {
                            input: glium::glutin::KeyboardInput {
                                virtual_keycode: Some(glium::glutin::VirtualKeyCode::Escape),
                                ..
                            },
                            ..
                        } => break 'render,
                        _ => (),
                    }
                }
                _ => (),
            };

            // Use the `winit` backend feature to convert the winit event to a conrod input.
            let input = match convert_event(event, &display) {
                None => continue,
                Some(input) => input,
            };

            // Handle the input with the `Ui`.
            ui.handle_event(input);

            // Set the widgets.
            let ui = &mut ui.set_widgets();

            // "Hello World!" in the middle of the screen.
            use conrod_core::{widget, Positionable, Colorable, Widget};
            widget::Text::new("Hello World!")
                .middle_of(ui.window)
                .color(conrod_core::color::WHITE)
                .font_size(39)
                .set(ids.text, ui);
        }

        // Draw the `Ui` if it has changed.
        if let Some(primitives) = ui.draw_if_changed() {
            renderer.fill(&display.0, primitives, &image_map);
            let mut target = display.0.draw();
            target.clear_color(0.0, 0.0, 1.0, 1.0);
            renderer.draw(&display.0, &mut target, &image_map).unwrap();
            target.finish().unwrap();
        }
    }

    println!("Hello, world!");
}
```

This shall be the reanimated corpse that we accessorize with our new widgets.
Let's replace that portion that just creates a Text widget with something a 
little bit fancier.

To start with, we'll add this code at the top of the file, before `fn main()`.

```rust
use conrod_core::{widget, Positionable, color, Colorable, Widget};

// Generate the widget identifiers.
widget_ids! {
    struct Ids {
        text,
        // maybe more?
    }
}

// Our new state struct
struct State {
    text: String,
}

fn gui(ui: &mut UiCell, ids: &Ids, state: &mut State) {
    widget::Text::new(&state.text)
        .middle_of(ui.window)
        .color(color::WHITE)
        .font_size(39)
        .set(ids.text, ui);
}
```

This is essentially all the same stuff that we had before,
we've just moved it out into its own function to keep things
clean.

Our `main` function now can simply call `gui()` in the event loop.

```rust
    // set up ui, ids and state
    // ...
    gui(&mut ui.set_widgets(), &ids, &mut state);
```

## Making the monster widget

Now we can dive into creating our custom widget.
I have three questions I ask when making my own widgets in conrod:

1. What does it do?
2. How should it look?
3. How does it change?

Question 1 means figuring out what the widget is *for* and how its state
should be laid out.
Question 2 means styling our widget and determining what its components
will be (our widget will be a composite of simpler widgets).
Question 3 means coming up with how the widget will react to user input
and allow modification.
Obviously, some widgets (like images) don't need any way of being changed,
so this really applies only to the fancier widgets.

[Rust]: https://rust-lang.org
[Conrod]: https://github.com/PistonDevelopers/conrod
[conrod_prompt]: https://gitlab.com/birdink/conrod_prompt
