+++
title = "Cranking out a GUI with Conrod"
description = "A series on Rust GUI development"
date = 2020-01-04

template = "page.html"

[taxonomies]
categories = ["programming"]
tags = ["rust", "gui", "conrod", "dev"]
+++

This is the landing for a series I've started on designing a GUI with Conrod.

You can reach the posts in order here:
* [Day 1](@/conrod/day-1.md)
* [Day 2](@/conrod/day-2.md)
